package ru.t1.ktitov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.data.DataJsonFasterXmlSaveRequest;
import ru.t1.ktitov.tm.event.ConsoleEvent;

@Component
public class DataJsonFasterXmlSaveListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-save-json-fasterxml";

    @NotNull
    public static final String DESCRIPTION = "Save data in json file by fasterxml";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonFasterXmlSaveListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull DataJsonFasterXmlSaveRequest request = new DataJsonFasterXmlSaveRequest(getToken());
        domainEndpoint.saveDataJsonFasterXml(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
