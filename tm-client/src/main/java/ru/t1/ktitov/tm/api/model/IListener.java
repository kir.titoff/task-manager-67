package ru.t1.ktitov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.event.ConsoleEvent;

public interface IListener {

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    @Nullable
    String getName();

    void execute(@NotNull final ConsoleEvent consoleEvent) throws Exception;

}
