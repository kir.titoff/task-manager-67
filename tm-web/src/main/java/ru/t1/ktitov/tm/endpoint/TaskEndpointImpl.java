package ru.t1.ktitov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktitov.tm.api.endpoint.ITaskRestEndpoint;
import ru.t1.ktitov.tm.model.Task;
import ru.t1.ktitov.tm.repository.TaskRepository;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.ktitov.tm.api.endpoint.ITaskRestEndpoint")
public final class TaskEndpointImpl implements ITaskRestEndpoint {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    @WebMethod
    @PostMapping("/create")
    public void create() {
        taskRepository.create();
    }

    @Override
    @WebMethod
    @PutMapping("/save")
    public void save(
            @WebParam(name = "task", partName = "task")
            @RequestBody final Task task
    ) {
        taskRepository.save(task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody final Task task
    ) {
        taskRepository.removeById(task.getId());
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        taskRepository.removeById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @WebMethod
    @GetMapping("/find/{id}")
    public Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return taskRepository.findById(id);
    }

}
