package ru.t1.ktitov.tm.configuration;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.t1.ktitov.tm")
@PropertySource("classpath:application.properties")
@EnableJpaRepositories("ru.t1.ktitov.tm.api.repository")
public class ApplicationConfiguration {

    @Bean
    @NotNull
    public DataSource dataSource(
            @Value("#{environment['database.driver']}") String databaseDriver,
            @Value("#{environment['database.url']}") String databaseUrl,
            @Value("#{environment['database.username']}") String databaseUser,
            @Value("#{environment['database.password']}") String databasePassword
    ) {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseDriver);
        dataSource.setUrl(databaseUrl);
        dataSource.setUsername(databaseUser);
        dataSource.setPassword(databasePassword);
        return dataSource;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactoryBean
    ) {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactoryBean.getObject());
        return transactionManager;
    }

    @Bean
    @NotNull
    LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource,
            @Value("#{environment['database.driver']}") String databaseDriver,
            @Value("#{environment['database.url']}") String databaseUrl,
            @Value("#{environment['database.username']}") String databaseUser,
            @Value("#{environment['database.password']}") String databasePassword,
            @Value("#{environment['database.dialect']}") String databaseDialect,
            @Value("#{environment['database.hbm2ddl']}") String databaseHbm2ddl,
            @Value("#{environment['database.show_sql']}") String databaseShowSql,
            @Value("#{environment['database.format_sql']}") String databaseFormatSql
    ) {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.ktitov.tm.model");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DRIVER, databaseDriver);
        properties.put(Environment.URL, databaseUrl);
        properties.put(Environment.USER, databaseUser);
        properties.put(Environment.PASS, databasePassword);
        properties.put(Environment.DIALECT, databaseDialect);
        properties.put(Environment.HBM2DDL_AUTO, databaseHbm2ddl);
        properties.put(Environment.SHOW_SQL, databaseShowSql);
        properties.put(Environment.FORMAT_SQL, databaseFormatSql);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

}
