package ru.t1.ktitov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.ktitov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskRestEndpoint {

    @WebMethod
    @PostMapping("/create")
    void create();

    @WebMethod
    @PutMapping("/save")
    void save(
            @WebParam(name = "task", partName = "task")
            @RequestBody final Task task
    );

    @WebMethod
    @DeleteMapping("/delete")
    void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody final Task task
    );

    @WebMethod
    @DeleteMapping("/delete/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    );

    @WebMethod
    @GetMapping("/findAll")
    Collection<Task> findAll();

    @WebMethod
    @GetMapping("/find/{id}")
    Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    );

}
