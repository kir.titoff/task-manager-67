package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.api.repository.IProjectRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @NotNull
    @Transactional
    public Project add(@Nullable final Project model) {
        repository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Transactional
    public Collection<Project> add(@Nullable final Collection<Project> models) {
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Transactional
    public Project update(@Nullable final Project model) {
        model.setUpdated(new Date());
        repository.saveAndFlush(model);
        return model;
    }

    @Nullable
    public List<Project> findAll() {
        return repository.findAll();
    }

    @Nullable
    public Project findOneById(@Nullable final String id) {
        return repository.findById(id).orElse(null);
    }

    @NotNull
    @Transactional
    public Project remove(@Nullable final Project model) {
        repository.delete(model);
        return model;
    }

    @NotNull
    @Transactional
    public Project removeById(@Nullable final String id) {
        Optional<Project> model = repository.findById(id);
        repository.deleteById(id);
        return model.get();
    }

    public Project create(
            @Nullable final String name
    ) {
        @NotNull final Project project = new Project();
        project.setName(name);
        add(project);
        return project;
    }

}
