package ru.t1.ktitov.tm.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.ktitov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public final class Project {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date updated = new Date();

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @NotNull
    @Column(name = "user_id")
    private String userId = "71187d89-af5a-4abf-8c32-31dab120a298";

    public Project(@NotNull final String name) {
        this.name = name;
    }

}
