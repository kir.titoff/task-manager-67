package ru.t1.ktitov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktitov.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.ktitov.tm.api.endpoint.IProjectRestEndpoint")
public final class ProjectEndpointImpl implements IProjectRestEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @WebMethod
    @PostMapping("/create")
    public void create() {
        projectService.create("New Project " + System.currentTimeMillis());
    }

    @Override
    @WebMethod
    @PutMapping("/save")
    public void save(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project
    ) {
        projectService.update(project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project
    ) {
        projectService.remove(project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        projectService.removeById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<Project> findAll() {
        return projectService.findAll();
    }

    @Override
    @WebMethod
    @GetMapping("/find/{id}")
    public Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return projectService.findOneById(id);
    }

}
