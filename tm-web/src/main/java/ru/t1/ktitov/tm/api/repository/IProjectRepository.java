package ru.t1.ktitov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.ktitov.tm.model.Project;

@Repository
public interface IProjectRepository extends JpaRepository<Project, String> {

    Project findFirstById(@NotNull final String id);

}
