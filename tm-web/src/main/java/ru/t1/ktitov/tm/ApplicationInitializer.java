package ru.t1.ktitov.tm;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.t1.ktitov.tm.configuration.ApplicationConfiguration;
import ru.t1.ktitov.tm.configuration.WebApplicationConfiguration;


/**
 * http://localhost:8080/ws/ProjectEndpoint?wsdl
 * http://localhost:8080/ws/TaskEndpoint?wsdl
 */
public final class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] { ApplicationConfiguration.class };
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] { WebApplicationConfiguration.class };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }

}
