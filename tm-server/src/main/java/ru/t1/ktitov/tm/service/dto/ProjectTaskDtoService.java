package ru.t1.ktitov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.service.dto.IProjectDtoService;
import ru.t1.ktitov.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.ktitov.tm.api.service.dto.ITaskDtoService;
import ru.t1.ktitov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ktitov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ktitov.tm.exception.field.EmptyProjectIdException;
import ru.t1.ktitov.tm.exception.field.EmptyTaskIdException;
import ru.t1.ktitov.tm.exception.field.EmptyUserIdException;
import ru.t1.ktitov.tm.dto.model.ProjectDTO;
import ru.t1.ktitov.tm.dto.model.TaskDTO;

import javax.transaction.Transactional;
import java.util.List;

@Service
public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @Override
    @SneakyThrows
    @Transactional
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyTaskIdException();
        if (projectService.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
        @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        taskService.update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyTaskIdException();
        if (projectService.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
        @Nullable final TaskDTO task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        taskService.update(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        @Nullable ProjectDTO project = projectService.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, projectId);
        for (final TaskDTO task : tasks) taskService.removeById(task.getId());
        projectService.removeById(userId, projectId);
        return project;
    }

}
