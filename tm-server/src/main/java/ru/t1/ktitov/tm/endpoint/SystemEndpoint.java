package ru.t1.ktitov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.ktitov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.dto.request.system.ServerAboutRequest;
import ru.t1.ktitov.tm.dto.request.system.ServerHostRequest;
import ru.t1.ktitov.tm.dto.request.system.ServerVersionRequest;
import ru.t1.ktitov.tm.dto.response.system.ServerAboutResponse;
import ru.t1.ktitov.tm.dto.response.system.ServerHostResponse;
import ru.t1.ktitov.tm.dto.response.system.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.net.UnknownHostException;

@Controller
@WebService(endpointInterface = "ru.t1.ktitov.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Override
    @WebMethod
    public ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerAboutRequest request
    ) {
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerVersionRequest request
    ) {
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

    @NotNull
    @WebMethod
    public ServerHostResponse getHost(@NotNull ServerHostRequest request) throws UnknownHostException {
        @NotNull final ServerHostResponse response = new ServerHostResponse();
        response.setHost(propertyService.getApplicationHost());
        return response;
    }

}
