package ru.t1.ktitov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.model.Task;
import ru.t1.ktitov.tm.model.User;

import java.util.List;

@Repository
public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Nullable
    List<Task> findByUser(@NotNull final User user);

    @Nullable
    List<Task> findByProject(@NotNull final User user);

    @Nullable
    @Query("SELECT t FROM Task t WHERE t.id = :id AND t.user = :user")
    Task findByIdAndUser(
            @NotNull @Param("user") final User user,
            @NotNull @Param("id") final String id
    );

    @Nullable
    @Query("SELECT t FROM Task t WHERE t.user = :user AND t.project = :project")
    List<Task> findByUserAndProject(
            @NotNull @Param("user") final User user,
            @NotNull @Param("project") final Project project
    );

    @Nullable
    @Query("SELECT t FROM Task t WHERE t.user = :user ORDER BY :orderBy")
    List<Task> findAll(
            @NotNull @Param("user") final User userId,
            @NotNull @Param("orderBy") final String orderBy
    );

    @Query("SELECT COUNT(1) = 1 FROM Task t WHERE t.id = :id AND t.user = :user")
    Boolean existsByIdAndUser(
            @NotNull @Param("user") final User user,
            @NotNull @Param("id") final String id
    );

}
